
import 'package:freezed_annotation/freezed_annotation.dart';

part 'password.freezed.dart';
part 'password.g.dart';

@freezed
abstract class Password with _$Password {
  factory Password() = _Password;
	
  factory Password.fromJson(Map<String, dynamic> json) =>
			_$PasswordFromJson(json);
}
