import 'package:cubit_form/cubit_form.dart';
import 'package:flutter/material.dart';
import 'package:project/aplicacion/Numeros_mayas.dart';
import 'package:project/components/Controlador.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => NumerosMayas(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: App()/*Scaffold(
          appBar: AppBar(
            title: const Text('arquitectura'),
          ),
          body: const Center(
            child: Componente(),
          ),*/
      ),
    );
  }
}

class App extends StatelessWidget {
  const App({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Números mayas'),
        backgroundColor: Colors.teal[900],
      ),
      body:
         Controlador()
    );
    
 }
}
