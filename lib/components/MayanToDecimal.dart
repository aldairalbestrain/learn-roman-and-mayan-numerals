import 'package:cubit_form/cubit_form.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project/aplicacion/Numeros_mayas.dart';
import 'package:project/aplicacion/estadopassword.dart';
import 'package:project/components/Draw_mayan.dart';

class MayanToDecimal extends StatelessWidget {
  const MayanToDecimal({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NumerosMayas, Estadopassword>(
      builder: (context, estado) {
        return Column(
          children: <Widget>[
            
            Row(children: [
              IconButton(
                icon: (Icon(Icons.loop)),
                tooltip: 'Cambiar de modo -> De arábigos - maya',
                onPressed: () {
                  context.bloc<NumerosMayas>().changueGame(estado.decilmalToMayan);                    
                },
              ),
            ],),
            Container(child: 
              Text('De maya - arábigos', 
                style: TextStyle(fontSize: 26, color: Colors.grey[700]),
              ), 
              margin: const EdgeInsets.only(bottom: 10.0)
            ),
            
            Pantalla(lista: estado.listaNumeroMaya, posicion: estado.posicion,), 
            Icon(
              estado.acierto ? Icons.check_circle_rounded : Icons.highlight_off_rounded,
              color: estado.acierto ? Colors.green : Colors.black, 
            ),   
            
            Text('Tienes que dibujar el número ${estado.numero}', style: TextStyle(fontStyle: FontStyle.italic)),

            //Botones posicion  
            Container(child: 
              Row(
              children: [
                Text('Nivel: '),
                IconButton(
                  color: estado.posicionMenos ?  Colors.black : Colors.black26,
                  icon: (Icon(Icons.exposure_minus_1)),
                  tooltip: 'Menos uno',
                  onPressed: estado.posicionMenos ? () {
                    context.bloc<NumerosMayas>().restarPosicion(estado.listaNumeros, estado.posicion);                    
                  } : (){},
                ),

                Container(child: Text('${estado.posicion}', style: TextStyle(fontSize: 20, ),),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.blue)
                  ),
                  padding: const EdgeInsets.all(8.0),
                ),
                
                IconButton(
                  color: estado.posicionMas ? Colors.black : Colors.black26,
                  icon: (Icon(Icons.plus_one)),
                  tooltip: 'Más uno',
                  onPressed: estado.posicionMas ? () {
                    context.bloc<NumerosMayas>().sumarPosicion(estado.listaNumeroMaya, estado.listaNumeros, estado.posicion);                    
                  } : null,
                ),

                IconButton(
                  color: estado.posicionDelete ? Colors.black : Colors.black26,
                  icon: (Icon(Icons.delete_forever_rounded)),
                  tooltip: 'Delete nivel',
                  onPressed: estado.posicionDelete ? () {
                    context.bloc<NumerosMayas>().eliminarPosicion(estado.listaNumeroMaya, estado.listaNumeros, estado.posicion);                    
                  } : null,
                ),
              ],
            ), 
            ),
            

            //Botones de control
            Row(
              children: [
                Text('+ Agregar'),
                IconButton(
                  color: estado.masVeinte ? Colors.black : Colors.black26,
                  icon: (Icon(Icons.cloud_circle_outlined)),
                  tooltip: 'Agregar 20',
                  onPressed: estado.masVeinte ? () {
                    context.bloc<NumerosMayas>().sumarNumero(estado.listaNumeroMaya, estado.listaNumeros, estado.posicion, 20);                    
                  } : null,
                ),

                IconButton(
                  color: estado.masCinco ? Colors.black : Colors.black26,
                  icon: (Icon(Icons.do_disturb_on_outlined)),
                  tooltip: 'Agregar 5',
                  onPressed: estado.masCinco ? () {
                    context.bloc<NumerosMayas>().sumarNumero(estado.listaNumeroMaya, estado.listaNumeros, estado.posicion, 5);                    
                  } : null,
                ),

                IconButton(
                  color: estado.masUno ? Colors.black : Colors.black26,
                  icon: (Icon(Icons.album_outlined)),
                  tooltip: 'Agregar 1',
                  onPressed: estado.masUno ? () {
                    context.bloc<NumerosMayas>().sumarNumero(estado.listaNumeroMaya, estado.listaNumeros, estado.posicion, 1);                    
                  } : null,
                ),
              ],
            ),

            Row(
              children: [
                Text('- Quitar'),
                IconButton(
                  color: estado.menosVeinte ? Colors.black : Colors.black26,
                  icon: (Icon(Icons.cloud_circle_outlined)),
                  tooltip: 'Quitar 20',
                  onPressed: estado.menosVeinte ? () {
                    context.bloc<NumerosMayas>().restarNumero(estado.listaNumeroMaya, estado.listaNumeros, estado.posicion, 20);                    
                  } : null,
                ),

                IconButton(
                  color: estado.menosCinco ? Colors.black : Colors.black26,
                  icon: (Icon(Icons.do_disturb_on_outlined)),
                  tooltip: 'Quitar 5',
                  onPressed: estado.menosCinco ? () {
                    context.bloc<NumerosMayas>().restarNumero(estado.listaNumeroMaya, estado.listaNumeros, estado.posicion, 5);                    
                  } : null,
                ),

                IconButton(
                  color: estado.menosUno ? Colors.black : Colors.black26,
                  icon: (Icon(Icons.album_outlined)),
                  tooltip: 'Quitar 1',
                  onPressed: estado.menosUno ? () {
                    context.bloc<NumerosMayas>().restarNumero(estado.listaNumeroMaya, estado.listaNumeros, estado.posicion, 1);                    
                  } : null,
                ),
              ],
            ),

            Expanded(
              child: 
              Row(
              children: [
                IconButton(
                  icon: (Icon(Icons.arrow_forward_ios)),
                  tooltip: 'Verificar',
                  onPressed: () {
                    context.bloc<NumerosMayas>().verificarMayaADecimal(estado.listaNumeros, estado.numero);
                  },
                ),

                IconButton(
                  icon: (Icon(Icons.sentiment_dissatisfied_sharp)),
                  tooltip: 'Rendirse :(',
                  onPressed: () {
                    showAlertDialog(context, 'Te rendiste :(', 'El número ${estado.numero} se dibuja así:', DrawMayan(number: estado.numero,));
                    context.bloc<NumerosMayas>().rendirse();                  
                  },
                )
              ],
            )
            )
          ]
        );
      }
    );
  }
}

class Pantalla extends StatelessWidget {
  const Pantalla({this.lista, this.posicion});
  final List<Widget> lista;
  final int posicion;

  @override
  Widget build(BuildContext context) {
    List<Widget> nuevoAcomodo = [];

    for(int i = 0; i < lista.length; i++){
      if (posicion == i)
        nuevoAcomodo.insert(0, Container(child: lista[i], decoration: BoxDecoration(color: Colors.blue[50]),));
      else
        nuevoAcomodo.insert(0, lista[i]);  
    }

    Column columna = Column(children: nuevoAcomodo,);
    Container contenedor = Container(child: columna, 
      width: 200,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black),
    ),);

    return contenedor;
  }
}


showAlertDialog(BuildContext context, String title ,String message, Widget widget) {
  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text(title),
    content: Column(children: [Text(message), widget],),
    actions: [
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}