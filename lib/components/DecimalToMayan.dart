import 'package:cubit_form/cubit_form.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project/aplicacion/Numeros_mayas.dart';
import 'package:project/aplicacion/estadopassword.dart';
import 'package:project/components/Draw_mayan.dart';

class DecimalToMayan extends StatelessWidget {
  const DecimalToMayan({this.contexto, this.estado});
  final contexto;
  final estado;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NumerosMayas, Estadopassword>(
      builder: (context, estado) {
        return Column(
          children: <Widget>[
            
            Row(children: [
              IconButton(
                icon: (Icon(Icons.loop)),
                tooltip: 'Cambiar de modo -> De maya - arábigo',
                onPressed: () {
                  context.bloc<NumerosMayas>().changueGame(estado.decilmalToMayan);                    
                },
              ),
            ],),
            
            Container(child: 
              Text('De arábigos - maya', 
                style: TextStyle(fontSize: 26, color: Colors.grey[700]),
              ), 
              margin: const EdgeInsets.only(bottom: 10.0)
            ),

            DrawMayan(number: estado.numero,),
            
            Icon(
              estado.acierto ? Icons.check_circle_rounded : Icons.highlight_off_rounded,
              color: estado.acierto ? Colors.green : Colors.black, 
            ),
            
            Expanded(
              child: 
              new TextField(
                decoration:InputDecoration(
                  hintText: 'Ingrese un número'
                ),
                onChanged: (text) {
                  context.bloc<NumerosMayas>().cambio(text);
                },
              ),  
            ),
            Expanded(
              child: 
              Row(
              children: [
                IconButton(
                  icon: (Icon(Icons.arrow_forward_ios)),
                  tooltip: 'Verificar',
                  onPressed: () {
                    context.bloc<NumerosMayas>().verificar(estado.numero, estado.valorTexto);
                  },
                ),

                IconButton(
                  icon: (Icon(Icons.sentiment_dissatisfied_sharp)),
                  tooltip: 'Rendirse :(',
                  onPressed: () {
                    showAlertDialog(context, 'Te rendiste :(', 'El número era ${estado.numero}');
                    context.bloc<NumerosMayas>().generarOtroNumero();                    
                  },
                )
              ],
            )
            )
          ],    
        );
      });
  }
}

showAlertDialog(BuildContext context, String title ,String message) {
  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text(title),
    content: Text(message),
    actions: [
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}