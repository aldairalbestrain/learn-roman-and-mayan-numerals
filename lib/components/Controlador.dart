import 'package:cubit_form/cubit_form.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project/aplicacion/Numeros_mayas.dart';
import 'package:project/aplicacion/estadopassword.dart';
import 'package:project/components/DecimalToMayan.dart';
import 'package:project/components/MayanToDecimal.dart';

class Controlador extends StatelessWidget {
  const Controlador({Key key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NumerosMayas, Estadopassword>(
      builder: (context, estado) {

        if (!estado.decilmalToMayan) {

          return Container(
            child: DecimalToMayan(contexto: context, estado: estado,), 
            padding: const EdgeInsets.only(left: 50.0, right: 50.0, top: 20.0)
            );

        } else {

          return Container(
            child: MayanToDecimal(),
            padding: const EdgeInsets.only(left: 50.0, right: 50.0, top: 20.0)
          ); 
        }
        
    });
  }
}

 