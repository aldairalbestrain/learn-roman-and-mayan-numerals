import 'package:flutter/material.dart';
import 'package:project/components/Dibujar1Al19Maya.dart';
import 'package:project/components/Dibujar20.dart';

class DrawMayan extends StatelessWidget {
  const DrawMayan({this.number});

  final int number;
  
  @override
  Widget build(BuildContext context) {
    List<Widget> lista = List();

    var numberDiv = number/20;

    if ( number % 20 != 0) {
      var numberDivFloor = numberDiv.floor();
      var numberResto = number%20;

      lista.insert(0, Dibujar1Al19Maya(numero: numberResto,));
      lista.insert(0, Dibujar1Al19Maya(numero: numberDivFloor));

    } else {

      //pintar concha abajo
      lista.insert(0, 
         Dibujar20()
      );

      if (numberDiv == 20) {
        //pintar otra concha arriba
        lista.insert(0, 
          Dibujar20()
        );
         lista.insert(0, Dibujar1Al19Maya(numero: 1));
      } else {
        lista.insert(0, Dibujar1Al19Maya(numero: numberDiv));
      }

    }

    Column renglon = Column(children: lista);

    return renglon;
  }
}