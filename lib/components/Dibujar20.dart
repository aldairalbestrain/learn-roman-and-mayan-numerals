import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Dibujar20 extends StatelessWidget {
  const Dibujar20({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.network('https://www.mexicodesconocido.com.mx/wp-content/uploads/2019/09/cero_maya-1024x640.png'),
      height: 40, 
      width: 250,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black)
      ),
    );
  }
}