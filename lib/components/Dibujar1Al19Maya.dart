import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Dibujar1Al19Maya extends StatelessWidget {
  const Dibujar1Al19Maya({this.numero});
  final numero;

  @override
  Widget build(BuildContext context) {
    List<Widget> listaCincos = List();
    List<Widget> listaUnos = List();
    var numberDiv = numero;
    
    //-------------------- Ciscos ------------------

        var numberDivCinco = (numberDiv/5).floor();
        if (numberDivCinco >= 1) {
          for (int i = 0; i < numberDivCinco; i++) {
            //agregar un 5
            listaCincos.insert(0, 
             Container(
               child: Image.network('https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Maya_5.svg/1024px-Maya_5.svg.png'),
               height: 40, 
               width: 150,
              )
          );
          }
          
          numberDiv = numberDiv - (numberDivCinco * 5);
        }

        //------------ unooss ----------------------------
        if (numberDiv >= 1) {
          for (int i= 0; i < numberDiv; i++) {
            //pintar punto
            listaUnos.insert(0, 
             Container(
               child: Image.network('https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/Maya_1.svg/1024px-Maya_1.svg.png'),
               height: 40, 
              )
          );
          }
          Row rowsUnos = Row(children: listaUnos, mainAxisAlignment: MainAxisAlignment.spaceEvenly,);
          Container containerUnos = Container( child: rowsUnos,);
          listaCincos.insert(0, containerUnos);
        }

        if(numero == 0) listaCincos.add(Container(height: 40,));

        //---------- resultados --------------
        Column columnTop = Column(children: listaCincos,);
        Container container = Container(
          child: columnTop, 
          width: 250, 
          decoration: BoxDecoration(
              border: Border.all(color: numero == 0 ? Colors.red : Colors.black)
            ),
          );


        return container;
  }
}