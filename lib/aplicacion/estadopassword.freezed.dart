// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'estadopassword.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$EstadopasswordTearOff {
  const _$EstadopasswordTearOff();

// ignore: unused_element
  _Estadopassword call(
      {@required String valorTexto,
      @required int numero,
      @required bool acierto,
      @required bool decilmalToMayan,
      @required List<Widget> listaNumeroMaya,
      @required List<int> listaNumeros,
      @required int posicion,
      @required bool masUno,
      @required bool masCinco,
      @required bool masVeinte,
      @required bool menosUno,
      @required bool menosCinco,
      @required bool menosVeinte,
      @required bool posicionMas,
      @required bool posicionMenos,
      @required bool posicionDelete}) {
    return _Estadopassword(
      valorTexto: valorTexto,
      numero: numero,
      acierto: acierto,
      decilmalToMayan: decilmalToMayan,
      listaNumeroMaya: listaNumeroMaya,
      listaNumeros: listaNumeros,
      posicion: posicion,
      masUno: masUno,
      masCinco: masCinco,
      masVeinte: masVeinte,
      menosUno: menosUno,
      menosCinco: menosCinco,
      menosVeinte: menosVeinte,
      posicionMas: posicionMas,
      posicionMenos: posicionMenos,
      posicionDelete: posicionDelete,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $Estadopassword = _$EstadopasswordTearOff();

/// @nodoc
mixin _$Estadopassword {
  String get valorTexto;
  int get numero;
  bool get acierto;
  bool get decilmalToMayan;
  List<Widget> get listaNumeroMaya;
  List<int> get listaNumeros;
  int get posicion;
  bool get masUno;
  bool get masCinco;
  bool get masVeinte;
  bool get menosUno;
  bool get menosCinco;
  bool get menosVeinte;
  bool get posicionMas;
  bool get posicionMenos;
  bool get posicionDelete;

  @JsonKey(ignore: true)
  $EstadopasswordCopyWith<Estadopassword> get copyWith;
}

/// @nodoc
abstract class $EstadopasswordCopyWith<$Res> {
  factory $EstadopasswordCopyWith(
          Estadopassword value, $Res Function(Estadopassword) then) =
      _$EstadopasswordCopyWithImpl<$Res>;
  $Res call(
      {String valorTexto,
      int numero,
      bool acierto,
      bool decilmalToMayan,
      List<Widget> listaNumeroMaya,
      List<int> listaNumeros,
      int posicion,
      bool masUno,
      bool masCinco,
      bool masVeinte,
      bool menosUno,
      bool menosCinco,
      bool menosVeinte,
      bool posicionMas,
      bool posicionMenos,
      bool posicionDelete});
}

/// @nodoc
class _$EstadopasswordCopyWithImpl<$Res>
    implements $EstadopasswordCopyWith<$Res> {
  _$EstadopasswordCopyWithImpl(this._value, this._then);

  final Estadopassword _value;
  // ignore: unused_field
  final $Res Function(Estadopassword) _then;

  @override
  $Res call({
    Object valorTexto = freezed,
    Object numero = freezed,
    Object acierto = freezed,
    Object decilmalToMayan = freezed,
    Object listaNumeroMaya = freezed,
    Object listaNumeros = freezed,
    Object posicion = freezed,
    Object masUno = freezed,
    Object masCinco = freezed,
    Object masVeinte = freezed,
    Object menosUno = freezed,
    Object menosCinco = freezed,
    Object menosVeinte = freezed,
    Object posicionMas = freezed,
    Object posicionMenos = freezed,
    Object posicionDelete = freezed,
  }) {
    return _then(_value.copyWith(
      valorTexto:
          valorTexto == freezed ? _value.valorTexto : valorTexto as String,
      numero: numero == freezed ? _value.numero : numero as int,
      acierto: acierto == freezed ? _value.acierto : acierto as bool,
      decilmalToMayan: decilmalToMayan == freezed
          ? _value.decilmalToMayan
          : decilmalToMayan as bool,
      listaNumeroMaya: listaNumeroMaya == freezed
          ? _value.listaNumeroMaya
          : listaNumeroMaya as List<Widget>,
      listaNumeros: listaNumeros == freezed
          ? _value.listaNumeros
          : listaNumeros as List<int>,
      posicion: posicion == freezed ? _value.posicion : posicion as int,
      masUno: masUno == freezed ? _value.masUno : masUno as bool,
      masCinco: masCinco == freezed ? _value.masCinco : masCinco as bool,
      masVeinte: masVeinte == freezed ? _value.masVeinte : masVeinte as bool,
      menosUno: menosUno == freezed ? _value.menosUno : menosUno as bool,
      menosCinco:
          menosCinco == freezed ? _value.menosCinco : menosCinco as bool,
      menosVeinte:
          menosVeinte == freezed ? _value.menosVeinte : menosVeinte as bool,
      posicionMas:
          posicionMas == freezed ? _value.posicionMas : posicionMas as bool,
      posicionMenos: posicionMenos == freezed
          ? _value.posicionMenos
          : posicionMenos as bool,
      posicionDelete: posicionDelete == freezed
          ? _value.posicionDelete
          : posicionDelete as bool,
    ));
  }
}

/// @nodoc
abstract class _$EstadopasswordCopyWith<$Res>
    implements $EstadopasswordCopyWith<$Res> {
  factory _$EstadopasswordCopyWith(
          _Estadopassword value, $Res Function(_Estadopassword) then) =
      __$EstadopasswordCopyWithImpl<$Res>;
  @override
  $Res call(
      {String valorTexto,
      int numero,
      bool acierto,
      bool decilmalToMayan,
      List<Widget> listaNumeroMaya,
      List<int> listaNumeros,
      int posicion,
      bool masUno,
      bool masCinco,
      bool masVeinte,
      bool menosUno,
      bool menosCinco,
      bool menosVeinte,
      bool posicionMas,
      bool posicionMenos,
      bool posicionDelete});
}

/// @nodoc
class __$EstadopasswordCopyWithImpl<$Res>
    extends _$EstadopasswordCopyWithImpl<$Res>
    implements _$EstadopasswordCopyWith<$Res> {
  __$EstadopasswordCopyWithImpl(
      _Estadopassword _value, $Res Function(_Estadopassword) _then)
      : super(_value, (v) => _then(v as _Estadopassword));

  @override
  _Estadopassword get _value => super._value as _Estadopassword;

  @override
  $Res call({
    Object valorTexto = freezed,
    Object numero = freezed,
    Object acierto = freezed,
    Object decilmalToMayan = freezed,
    Object listaNumeroMaya = freezed,
    Object listaNumeros = freezed,
    Object posicion = freezed,
    Object masUno = freezed,
    Object masCinco = freezed,
    Object masVeinte = freezed,
    Object menosUno = freezed,
    Object menosCinco = freezed,
    Object menosVeinte = freezed,
    Object posicionMas = freezed,
    Object posicionMenos = freezed,
    Object posicionDelete = freezed,
  }) {
    return _then(_Estadopassword(
      valorTexto:
          valorTexto == freezed ? _value.valorTexto : valorTexto as String,
      numero: numero == freezed ? _value.numero : numero as int,
      acierto: acierto == freezed ? _value.acierto : acierto as bool,
      decilmalToMayan: decilmalToMayan == freezed
          ? _value.decilmalToMayan
          : decilmalToMayan as bool,
      listaNumeroMaya: listaNumeroMaya == freezed
          ? _value.listaNumeroMaya
          : listaNumeroMaya as List<Widget>,
      listaNumeros: listaNumeros == freezed
          ? _value.listaNumeros
          : listaNumeros as List<int>,
      posicion: posicion == freezed ? _value.posicion : posicion as int,
      masUno: masUno == freezed ? _value.masUno : masUno as bool,
      masCinco: masCinco == freezed ? _value.masCinco : masCinco as bool,
      masVeinte: masVeinte == freezed ? _value.masVeinte : masVeinte as bool,
      menosUno: menosUno == freezed ? _value.menosUno : menosUno as bool,
      menosCinco:
          menosCinco == freezed ? _value.menosCinco : menosCinco as bool,
      menosVeinte:
          menosVeinte == freezed ? _value.menosVeinte : menosVeinte as bool,
      posicionMas:
          posicionMas == freezed ? _value.posicionMas : posicionMas as bool,
      posicionMenos: posicionMenos == freezed
          ? _value.posicionMenos
          : posicionMenos as bool,
      posicionDelete: posicionDelete == freezed
          ? _value.posicionDelete
          : posicionDelete as bool,
    ));
  }
}

/// @nodoc
class _$_Estadopassword implements _Estadopassword {
  _$_Estadopassword(
      {@required this.valorTexto,
      @required this.numero,
      @required this.acierto,
      @required this.decilmalToMayan,
      @required this.listaNumeroMaya,
      @required this.listaNumeros,
      @required this.posicion,
      @required this.masUno,
      @required this.masCinco,
      @required this.masVeinte,
      @required this.menosUno,
      @required this.menosCinco,
      @required this.menosVeinte,
      @required this.posicionMas,
      @required this.posicionMenos,
      @required this.posicionDelete})
      : assert(valorTexto != null),
        assert(numero != null),
        assert(acierto != null),
        assert(decilmalToMayan != null),
        assert(listaNumeroMaya != null),
        assert(listaNumeros != null),
        assert(posicion != null),
        assert(masUno != null),
        assert(masCinco != null),
        assert(masVeinte != null),
        assert(menosUno != null),
        assert(menosCinco != null),
        assert(menosVeinte != null),
        assert(posicionMas != null),
        assert(posicionMenos != null),
        assert(posicionDelete != null);

  @override
  final String valorTexto;
  @override
  final int numero;
  @override
  final bool acierto;
  @override
  final bool decilmalToMayan;
  @override
  final List<Widget> listaNumeroMaya;
  @override
  final List<int> listaNumeros;
  @override
  final int posicion;
  @override
  final bool masUno;
  @override
  final bool masCinco;
  @override
  final bool masVeinte;
  @override
  final bool menosUno;
  @override
  final bool menosCinco;
  @override
  final bool menosVeinte;
  @override
  final bool posicionMas;
  @override
  final bool posicionMenos;
  @override
  final bool posicionDelete;

  @override
  String toString() {
    return 'Estadopassword(valorTexto: $valorTexto, numero: $numero, acierto: $acierto, decilmalToMayan: $decilmalToMayan, listaNumeroMaya: $listaNumeroMaya, listaNumeros: $listaNumeros, posicion: $posicion, masUno: $masUno, masCinco: $masCinco, masVeinte: $masVeinte, menosUno: $menosUno, menosCinco: $menosCinco, menosVeinte: $menosVeinte, posicionMas: $posicionMas, posicionMenos: $posicionMenos, posicionDelete: $posicionDelete)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Estadopassword &&
            (identical(other.valorTexto, valorTexto) ||
                const DeepCollectionEquality()
                    .equals(other.valorTexto, valorTexto)) &&
            (identical(other.numero, numero) ||
                const DeepCollectionEquality().equals(other.numero, numero)) &&
            (identical(other.acierto, acierto) ||
                const DeepCollectionEquality()
                    .equals(other.acierto, acierto)) &&
            (identical(other.decilmalToMayan, decilmalToMayan) ||
                const DeepCollectionEquality()
                    .equals(other.decilmalToMayan, decilmalToMayan)) &&
            (identical(other.listaNumeroMaya, listaNumeroMaya) ||
                const DeepCollectionEquality()
                    .equals(other.listaNumeroMaya, listaNumeroMaya)) &&
            (identical(other.listaNumeros, listaNumeros) ||
                const DeepCollectionEquality()
                    .equals(other.listaNumeros, listaNumeros)) &&
            (identical(other.posicion, posicion) ||
                const DeepCollectionEquality()
                    .equals(other.posicion, posicion)) &&
            (identical(other.masUno, masUno) ||
                const DeepCollectionEquality().equals(other.masUno, masUno)) &&
            (identical(other.masCinco, masCinco) ||
                const DeepCollectionEquality()
                    .equals(other.masCinco, masCinco)) &&
            (identical(other.masVeinte, masVeinte) ||
                const DeepCollectionEquality()
                    .equals(other.masVeinte, masVeinte)) &&
            (identical(other.menosUno, menosUno) ||
                const DeepCollectionEquality()
                    .equals(other.menosUno, menosUno)) &&
            (identical(other.menosCinco, menosCinco) ||
                const DeepCollectionEquality()
                    .equals(other.menosCinco, menosCinco)) &&
            (identical(other.menosVeinte, menosVeinte) ||
                const DeepCollectionEquality()
                    .equals(other.menosVeinte, menosVeinte)) &&
            (identical(other.posicionMas, posicionMas) ||
                const DeepCollectionEquality()
                    .equals(other.posicionMas, posicionMas)) &&
            (identical(other.posicionMenos, posicionMenos) ||
                const DeepCollectionEquality()
                    .equals(other.posicionMenos, posicionMenos)) &&
            (identical(other.posicionDelete, posicionDelete) ||
                const DeepCollectionEquality()
                    .equals(other.posicionDelete, posicionDelete)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(valorTexto) ^
      const DeepCollectionEquality().hash(numero) ^
      const DeepCollectionEquality().hash(acierto) ^
      const DeepCollectionEquality().hash(decilmalToMayan) ^
      const DeepCollectionEquality().hash(listaNumeroMaya) ^
      const DeepCollectionEquality().hash(listaNumeros) ^
      const DeepCollectionEquality().hash(posicion) ^
      const DeepCollectionEquality().hash(masUno) ^
      const DeepCollectionEquality().hash(masCinco) ^
      const DeepCollectionEquality().hash(masVeinte) ^
      const DeepCollectionEquality().hash(menosUno) ^
      const DeepCollectionEquality().hash(menosCinco) ^
      const DeepCollectionEquality().hash(menosVeinte) ^
      const DeepCollectionEquality().hash(posicionMas) ^
      const DeepCollectionEquality().hash(posicionMenos) ^
      const DeepCollectionEquality().hash(posicionDelete);

  @JsonKey(ignore: true)
  @override
  _$EstadopasswordCopyWith<_Estadopassword> get copyWith =>
      __$EstadopasswordCopyWithImpl<_Estadopassword>(this, _$identity);
}

abstract class _Estadopassword implements Estadopassword {
  factory _Estadopassword(
      {@required String valorTexto,
      @required int numero,
      @required bool acierto,
      @required bool decilmalToMayan,
      @required List<Widget> listaNumeroMaya,
      @required List<int> listaNumeros,
      @required int posicion,
      @required bool masUno,
      @required bool masCinco,
      @required bool masVeinte,
      @required bool menosUno,
      @required bool menosCinco,
      @required bool menosVeinte,
      @required bool posicionMas,
      @required bool posicionMenos,
      @required bool posicionDelete}) = _$_Estadopassword;

  @override
  String get valorTexto;
  @override
  int get numero;
  @override
  bool get acierto;
  @override
  bool get decilmalToMayan;
  @override
  List<Widget> get listaNumeroMaya;
  @override
  List<int> get listaNumeros;
  @override
  int get posicion;
  @override
  bool get masUno;
  @override
  bool get masCinco;
  @override
  bool get masVeinte;
  @override
  bool get menosUno;
  @override
  bool get menosCinco;
  @override
  bool get menosVeinte;
  @override
  bool get posicionMas;
  @override
  bool get posicionMenos;
  @override
  bool get posicionDelete;
  @override
  @JsonKey(ignore: true)
  _$EstadopasswordCopyWith<_Estadopassword> get copyWith;
}
