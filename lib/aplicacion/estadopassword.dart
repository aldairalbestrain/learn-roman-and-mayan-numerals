import 'package:flutter/cupertino.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:project/components/Dibujar1Al19Maya.dart';
import 'package:project/components/Dibujar20.dart';

part 'estadopassword.freezed.dart';
//part 'estadopassword.g.dart';

@freezed
abstract class Estadopassword with _$Estadopassword {
  factory Estadopassword({
    @required String valorTexto,
    @required int numero,
    @required bool acierto,
    @required bool decilmalToMayan,
    @required List<Widget> listaNumeroMaya,
    @required List<int> listaNumeros,
    @required int posicion,
    @required bool masUno,
    @required bool masCinco,
    @required bool masVeinte,
    @required bool menosUno,
    @required bool menosCinco,
    @required bool menosVeinte,
    @required bool posicionMas,
    @required bool posicionMenos,
    @required bool posicionDelete,
  }) = _Estadopassword;

  factory Estadopassword.inicial() => Estadopassword(
    valorTexto: '',
    numero: 1,
    acierto: false,
    decilmalToMayan: true,
    listaNumeroMaya: [Dibujar1Al19Maya(numero: 0)],
    listaNumeros: [0],
    posicion: 0,
    masUno: true,
    masCinco: true,
    masVeinte: true,
    menosUno: true,
    menosCinco: true,
    menosVeinte: true,
    posicionMas: true,
    posicionMenos: true,
    posicionDelete: true
  );

  /*factory Estadopassword.fromJson(Map<String, dynamic> json) =>
      _$EstadopasswordFromJson(json);*/
}
