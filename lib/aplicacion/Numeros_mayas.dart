import 'dart:async';
import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project/components/Dibujar1Al19Maya.dart';
import 'package:project/components/Dibujar20.dart';
import 'estadopassword.dart';


class  NumerosMayas extends Cubit<Estadopassword> {
  NumerosMayas():super(Estadopassword.inicial()) {
    reiniciarEstados();
  }

  ///  -------------- DECIMAL A MAYA ---------------
  
  String input = '';
  

  void cambio(String texto) {
    final r = texto;

    emit(state.copyWith(valorTexto: r));
    print('valor de input: $r');
  }

  void verificar(int number, String estadoTexto) {
    final numeroIngresado = int.parse(estadoTexto);

    if (number == numeroIngresado) {
      emit(state.copyWith(acierto: true));
       Timer(Duration(seconds: 2), () {
        generarOtroNumero(); 
        emit(state.copyWith(acierto: false));
       });
      //timer.cancel();
    } else {
      emit(state.copyWith(acierto: false));
    }
  }

// ------------------ Maya a decimal 2 ------------------

  void verificarMayaADecimal(List<int> numeros, int numeroAComparar){
    int numero = 0;

    if (numeros[0] == 20) {
      numero = 1;
      for (int i = 0; i < numeros.length; i++) {
        numero = numero * numeros[i];
      }

    } else {
      int valorMultiplicado = 0;
      int multiplicador = 1;
      for (int i = 0; i < numeros.length; i++) {
        valorMultiplicado = numeros[i] * multiplicador;
        numero = numero + valorMultiplicado;
        multiplicador = 20;
      }
    }

    if (numero == numeroAComparar) {
      emit(state.copyWith(acierto: true));
      Timer(Duration(seconds: 2), () {
        emit(state.copyWith(acierto: false, listaNumeroMaya: [Dibujar1Al19Maya(numero: 0)], listaNumeros: [0], posicion: 0));
        reiniciarEstados();
       });
       
    } else {
      emit(state.copyWith(acierto: false));
    }
  }

  void reiniciarEstados() {
    var numeroRandom = new Random();
    emit(state.copyWith(numero: numeroRandom.nextInt(400) + 1));

    validacionesDeBtnParaSumarAlMaya([0], 0);
    validacionPosicionMas([0], 0);
    validacionPosicionMenos([0], 0);
    validacionPosicionDelete([0]);
  }

  void rendirse(){
    Timer(Duration(seconds: 2), () {
      emit(state.copyWith(acierto: false, listaNumeroMaya: [Dibujar1Al19Maya(numero: 0)], listaNumeros: [0], posicion: 0));
      reiniciarEstados();
      });
  }

  void sumarPosicion(List<Widget> listaDeWidgets, List<int> listaNumeros, int posicion){
    List<Widget> listaWidget = new List<Widget>.from(listaDeWidgets);
    List<int> numeros = new List<int>.from(listaNumeros);

    if (posicion == listaNumeros.length-1) {
      listaWidget.add(Dibujar1Al19Maya(numero: 0));
      numeros.add(0);
    }
    
    emit(state.copyWith(posicion: posicion + 1, listaNumeroMaya: listaWidget, listaNumeros: numeros));

    validacionesDeBtnParaSumarAlMaya(numeros, posicion + 1);
    validacionPosicionMas(numeros, posicion + 1);
    validacionPosicionMenos(numeros, posicion + 1);
    validacionPosicionDelete(numeros);
  }

  void restarPosicion(List<int> listaNumeros, int posicion){
    emit(state.copyWith(posicion: posicion - 1));

    validacionesDeBtnParaSumarAlMaya(listaNumeros, posicion -1);
    validacionPosicionMas(listaNumeros, posicion -1);
    validacionPosicionMenos(listaNumeros, posicion -1);
    validacionPosicionDelete(listaNumeros);
  }

  void eliminarPosicion(List<Widget> listaDeWidgets, List<int> listaNumeros, int posicion) {
    List<Widget> widgets = new List<Widget>.from(listaDeWidgets);
    widgets.removeAt(posicion);

    List<int> numeros = List<int>.from(listaNumeros);
    numeros.removeAt(posicion);

    int nuevaPosicion = posicion == 0 ? posicion : posicion - 1;

    emit(state.copyWith(listaNumeroMaya: widgets, listaNumeros: numeros, posicion: nuevaPosicion));
    validacionesDeBtnParaSumarAlMaya(numeros, nuevaPosicion);
    validacionPosicionMas(numeros, nuevaPosicion);
    validacionPosicionMenos(numeros, nuevaPosicion);
    validacionPosicionDelete(numeros);
  }

// -----------------VALIDACIONES ------------------------
  //Butons +
  void validacionMasVeinte(List<int> listaNumeros, int posicion){
    if (listaNumeros[posicion] >= 1 || posicion == 2/*|| listaNumeros[posicion - 1] != 20*/) ///////////validarbieeeeeeeeeeeeeeeeeeeeen
      emit(state.copyWith(masVeinte: false));
    else  
      emit(state.copyWith(masVeinte: true));
  }

  void validacionMascinco(List<int> listaNumeros, int posicion){
    if (listaNumeros[posicion] >= 15)
      emit(state.copyWith(masCinco: false));
    else  
      emit(state.copyWith(masCinco: true));
  }

  void validacionMasUno(List<int> listaNumeros, int posicion){
    if (listaNumeros[posicion] >= 19)
      emit(state.copyWith(masUno: false));
    else  
      emit(state.copyWith(masUno: true));
  }

  //Butons -
  void validacionMenosVeinte(List<int> listaNumeros, int posicion){
    if (listaNumeros[posicion] != 20) 
      emit(state.copyWith(menosVeinte: false));
    else  
      emit(state.copyWith(menosVeinte: true));
  }

  void validacionMenoscinco(List<int> listaNumeros, int posicion){
    if (listaNumeros[posicion] <= 4 || listaNumeros[posicion] >= 20)
      emit(state.copyWith(menosCinco: false));
    else  
      emit(state.copyWith(menosCinco: true));
  }

  void validacionMenosUno(List<int> listaNumeros, int posicion){
    if (listaNumeros[posicion] <= 0 || listaNumeros[posicion] >= 20)
      emit(state.copyWith(menosUno: false));
    else  
      emit(state.copyWith(menosUno: true));
  }

  //ejecutar validaciones de botones sumar y restar
  void validacionesDeBtnParaSumarAlMaya(List<int> listaNumeros, int posicion){
    validacionMasVeinte(listaNumeros, posicion);
    validacionMascinco(listaNumeros, posicion);
    validacionMasUno(listaNumeros, posicion);

    validacionMenosVeinte(listaNumeros, posicion);
    validacionMenoscinco(listaNumeros, posicion);
    validacionMenosUno(listaNumeros, posicion);
  }

  void validacionPosicionMenos(List<int> listaNumeros, int posicion){
    if (posicion == 0)
      emit(state.copyWith(posicionMenos: false));
    else  
      emit(state.copyWith(posicionMenos: true));
  }

  void validacionPosicionMas(List<int> listaNumeros, int posicion){
    if (listaNumeros[posicion] == 0 || posicion == 2)
      emit(state.copyWith(posicionMas: false));
    else  
      emit(state.copyWith(posicionMas: true));
  }

  void validacionPosicionDelete(List<int> listaNumeros) {
    if (listaNumeros.length == 1) 
      emit(state.copyWith(posicionDelete: false));
    else
      emit(state.copyWith(posicionDelete: true));
  }
//-------------------- METODOS GENERALES -----------------------

  void generarOtroNumero(){
    var numeroRandom = new Random();
    emit(state.copyWith(numero: numeroRandom.nextInt(400) + 1));
  }

  void changueGame(bool estado) {
    var numeroRandom = new Random();
    emit(state.copyWith(decilmalToMayan: !estado, numero: numeroRandom.nextInt(400) + 1));
  }
  
  //---------------METODOS PARA SUMAR----------------------------
  void checarEstadosDeBotones(List<int> listaDeNumeros ,int posicion){
    validacionesDeBtnParaSumarAlMaya(listaDeNumeros, posicion);
    validacionPosicionMas(listaDeNumeros, posicion);
    validacionPosicionMenos(listaDeNumeros, posicion);
    //validacionPosicionMas(listaDeNumeros, posicion);
    //validacionPosicionMenos(listaDeNumeros, posicion);
    //validacionPosicionMenos(listaDeNumeros, posicion);
    //validacionPosicionMas(listaDeNumeros, posicion);
    validacionPosicionDelete(listaDeNumeros);

  }

  void sumarNumero(List<Widget> listaDeWidgets, List<int> listaDeNumeros ,int posicion, int cantidad) {
     List<int> numeros = new List<int>.from(listaDeNumeros);
    numeros[posicion] = numeros[posicion] + cantidad;

    List<Widget> widgets = new List<Widget>.from(listaDeWidgets);

    if (numeros[posicion] == 20) {
      if (posicion == listaDeWidgets.length-1) {
        widgets[posicion] = Dibujar20();
        numeros.add(0);
        widgets.add(Dibujar1Al19Maya(numero: 0));
      } else {
        widgets[posicion] = Dibujar20();
      }
      
      int posicionNueva = numeros[posicion] == 20 ? posicion : posicion + 1;
      emit(state.copyWith(posicion: posicionNueva));
      sumarPosicion(listaDeWidgets,listaDeNumeros, posicionNueva);
      checarEstadosDeBotones(numeros, posicion + 1 );
    } else {
      widgets[posicion] = Dibujar1Al19Maya(numero: numeros[posicion]);
      checarEstadosDeBotones(numeros, posicion);
    }

    emit(state.copyWith(listaNumeros: numeros, listaNumeroMaya: widgets));
  }

  void restarNumero(List<Widget> listaDeWidgets, List<int> listaDeNumeros ,int posicion, int cantidad) {
     List<int> numeros = new List<int>.from(listaDeNumeros);
    numeros[posicion] = numeros[posicion] - cantidad;

    List<Widget> widgets = new List<Widget>.from(listaDeWidgets);

    if (numeros[posicion] == 20) {
      numeros.removeAt(posicion);
      widgets.removeAt(posicion);
      emit(state.copyWith(posicion: posicion + 1));
      checarEstadosDeBotones(numeros, posicion +1 );
    } else {
      widgets[posicion] = Dibujar1Al19Maya(numero: numeros[posicion]);
      checarEstadosDeBotones(numeros, posicion);
    }

    emit(state.copyWith(listaNumeros: numeros, listaNumeroMaya: widgets));
  }
}